#!/usr/bin/python3
"""
send DDC/CI commands

Copyright (C) 2016, 2017 Johannes Stezenbach

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
"""

import sys
import os
import time
import struct
import ctypes
import fcntl
import argparse
import codecs
import re
from pathlib import Path

# ioctl definition
I2C_RDWR = 0x0707

# i2c addresses
EDID_ADDR = 0x50
DDCCI_ADDR = 0x37

MAGIC_1	= 0x51	  # first byte to send, host address
MAGIC_2	= 0x80	  # second byte to send, ored with length
MAGIC_XOR = 0x50  # initial xor for received frame

DELAY_S = 50e-3
DELAYW_S = 100e-3


# CCD/CI commands
DDCCI_COMMAND_READ	   = 0x01	# read ctrl value
DDCCI_REPLY_READ	   = 0x02	# read ctrl value reply
DDCCI_COMMAND_WRITE	   = 0x03	# write ctrl value

DDCCI_COMMAND_PAGED_READ   = 0xc6	# read ctrl value from extension page
DDCCI_REPLY_PAGED_READ	   = 0xc7	# read ctrl value reply
DDCCI_COMMAND_PAGED_WRITE  = 0xc8	# write ctrl value to extension page

DDCCI_COMMAND_SAVE	   = 0x0c	# save current settings

DDCCI_COMMAND_CAPS	   = 0xf3	# get monitor caps
DDCCI_REPLY_CAPS	   = 0xe3	# get monitor caps reply

# control numbers
DDCCI_CTRL_BRIGHTNESS = 0x10

def eprint(*args):
    print(*args, file=sys.stderr)

class i2c_rdwr_ioctl_data(ctypes.Structure):
    _fields_ = [
        ( 'msgs', ctypes.c_void_p ),
        ( 'nmsgs', ctypes.c_uint )
    ]

class i2c_msg(ctypes.Structure):
    _fields_ = [
        ( 'addr', ctypes.c_ushort ),
        ( 'flags', ctypes.c_ushort ),
        ( 'len', ctypes.c_ushort ),
        ( 'buf', ctypes.c_char_p )
    ]


def i2c_read(fd, addr, length):
    buf = ctypes.create_string_buffer(length)
    msg = i2c_msg()
    msg.addr = addr
    msg.flags = 0x01 # I2C_M_RD
    msg.len = length
    msg.buf = ctypes.addressof(buf)
    data = i2c_rdwr_ioctl_data()
    data.nmsgs = 1
    data.msgs = ctypes.addressof(msg)
    rc = fcntl.ioctl(fd, I2C_RDWR, data, True)
    if rc != 1:
        raise IOError('i2c read failed')
    return buf.raw

def i2c_write(fd, addr, buf):
    msg = i2c_msg()
    msg.addr = addr  # EDID
    msg.flags = 0x00 # I2C_M_WR
    msg.len = len(buf)
    msg.buf = ctypes.addressof(buf)
    data = i2c_rdwr_ioctl_data()
    data.nmsgs = 1
    data.msgs = ctypes.addressof(msg)
    rc = fcntl.ioctl(fd, I2C_RDWR, data)
    if rc != 1:
        raise IOError('i2c write failed')


class DDC:
    def __init__(self, fd, verbose=0):
        self.fd = fd
        self.last = 0
        self.v = verbose

    def delay(self, iswrite):
        now = time.time()
        delay = self.last + DELAY_S - now
        if delay > 0:
            time.sleep(delay)
        if iswrite:
            self.last = now

    def read(self, length):
        self.delay(False)
        msg = i2c_read(self.fd, DDCCI_ADDR, length + 3)
        if self.v > 1:
            eprint("R", len(msg), codecs.encode(msg, 'hex'))
        if msg[0] != DDCCI_ADDR << 1:
            raise IOError("invalid response: %02x != %02x" % (msg[0], DDCCI_ADDR >> 1))
        if msg[1] & MAGIC_2 == 0:
            raise IOError("invalid response (magic 2): %02x " % msg[1])
        l = msg[1] & ~MAGIC_2
        if l > length or l + 3 > len(msg):
            raise IOError("invalid response, length %d > %d" % (l, length))
        xor = MAGIC_XOR
        for i in range(0, l + 3):
            xor ^= msg[i]
        if xor != 0:
            raise IOError("invalid response, checksum error %d %02x" % (l, xor))
        return msg[2:l + 2]

    def write(self, msg):
        self.delay(True)
        buf = ctypes.create_string_buffer(len(msg) + 3)
        xor = DDCCI_ADDR << 1
        buf[0] = MAGIC_1
        xor ^= ord(buf[0])
        buf[1] = MAGIC_2 | len(msg)
        xor ^= ord(buf[1])
        i = 2
        for c in msg:
            buf[i] = c
            xor ^= c
            i += 1
        buf[i] = xor
        if self.v > 1:
            eprint("W", codecs.encode(buf.raw, 'hex'))
        return i2c_write(self.fd, DDCCI_ADDR, buf)

    def command(self, cmd):
        msg = struct.pack("B", cmd)
        self.write(msg)

    def _write_ctrl(self, page, ctrl, value):
        tsf = Path(timestamp_file)
        try:
            s = tsf.stat()
            delay = s.st_mtime + DELAYW_S - time.time()
            if delay > 0:
                time.sleep(delay)
        except:
            pass
        if page:
            msg = struct.pack(">BBBH", DDCCI_COMMAND_PAGED_WRITE, page, ctrl, value)
        else:
            msg = struct.pack(">BBH", DDCCI_COMMAND_WRITE, ctrl, value)
        self.write(msg)
        tsf.touch()

    def _read_ctrl(self, page, ctrl):
        if page:
            return self._read_paged_ctrl(page, ctrl)
        msg = struct.pack("BB", DDCCI_COMMAND_READ, ctrl)
        self.write(msg)
        reply = self.read(8)
        if len(reply) != 8:
            raise IOError("read cmd reply too short")
        reply = struct.unpack(">BBBBHH", reply)
        if reply[0] != DDCCI_REPLY_READ or reply[2] != ctrl: 
            raise IOError("invalid read cmd reply")
        if reply[1] != 0: 
            raise IndexError("unsupported ctrl")
        return reply[5], reply[4]  # value, maximum

    def _read_paged_ctrl(self, page, ctrl):
        msg = struct.pack("BBB", DDCCI_COMMAND_PAGED_READ, page, ctrl)
        self.write(msg)
        reply = self.read(9)
        if len(reply) != 9:
            raise IOError("read cmd reply too short")
        reply = struct.unpack(">BBBBBHH", reply)
        if reply[0] != DDCCI_REPLY_PAGED_READ or reply[2] != page or reply[3] != ctrl: 
            raise IOError("invalid read cmd reply")
        if reply[1] != 0: 
            raise IndexError("unsupported ctrl")
        return reply[6], reply[5]  # value, maximum

    def write_ctrl(self, page, ctrl, value):
        ex = None
        for i in range(3):
            try:
                if i > 0 and self.v:
                    eprint("write_ctrl retry")
                self._write_ctrl(page, ctrl, value)
                return
            except IOError as e:
                ex = e
        raise ex

    def read_ctrl(self, page, ctrl):
        ex = None
        for i in range(3):
            try:
                if i > 0 and self.v:
                    eprint("read_ctrl retry")
                return self._read_ctrl(page, ctrl)
            except IOError as e:
                ex = e
        raise ex

    def caps(self):
        offset = 0
        c = b''
        retry = 0
        while True:
            msg = struct.pack(">BH", DDCCI_COMMAND_CAPS, offset)
            retry += 1
            try:
                self.write(msg)
                reply = self.read(64)
                if len(reply) <= 3:
                    break
                rc, ro = struct.unpack(">BH", reply[:3])
                if rc != DDCCI_REPLY_CAPS or ro != offset:
                    raise IOError("invalid caps cmd reply")
                c += reply[3:]
                offset += len(reply) - 3
                retry = 0
            except IOError as e:
                if retry > 3:
                    raise
                if self.v:
                    eprint("caps retry")
        return c

    def parse_caps(self, caps):
        res = []
        vcp_re = r'''\( (
                    (?: (?: [^()]+) (?:\( [^()]+ \))? (?: [^()]+) )+
                           ) \) '''
        pg0 = 0, re.search('vcp' + vcp_re, str(caps), re.VERBOSE)
        pg2 = 2, re.search('vcp_p0?2' + vcp_re, str(caps), re.VERBOSE)
        pg10 = 0x10, re.search('vcp_p10' + vcp_re, str(caps), re.VERBOSE)
        for p, v in pg0, pg2, pg10:
            if v:
                vcp = re.sub(r'\([^()]+\)', '', v.group(1))
                res.append([p, [int(c, 16) for c in vcp.split()]])
        return res


def auto_int(x):
    return int(x, 0)
parser = argparse.ArgumentParser(description='Read or write DDC/CI controls.')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-l', action='store_true', help='list i2c devices')
group.add_argument('-d', type=Path, help='i2c device', metavar='dev')
group.add_argument('-n', help='i2c device name (search i2c device via /sys)', metavar='name')
parser.add_argument('-c', action='store_true', help='show caps')
parser.add_argument('-e', action='store_true', help='dump EDID')
parser.add_argument('--dump', action='store_true', help='dump values of all controls')
parser.add_argument('-v', action='count', default=0, help='be verbose (repeat for more)')
parser.add_argument('-p', type=auto_int, help='extension page number', metavar='page', default=0)
parser.add_argument('control', type=auto_int, nargs='?', help='DDC/CI control number')
parser.add_argument('value', type=auto_int, nargs='?', help='DDC/CI control value')
parser.add_argument('-i', type=auto_int,
                    help='DDC/CI control increment (can be negatvie)', metavar='increment')
args = parser.parse_args()

if args.l or args.n:
    try:
        os.chdir('/sys/class/i2c-dev/')
    except FileNotFoundError:
        eprint('error: no i2c-dev devices', file=sys.stderr)
        sys.exit(1)
    for d in Path('.').glob('i2c-*'):
        n = (d / 'name').open().readline().rstrip()
        if args.l:
            print('%-6s %s' % (d, n))
        elif n == args.n:
            args.d = Path('/dev', d)
            break
    if args.l:
        sys.exit(0)
    if args.d is None:
        eprint('error: no i2c-dev device "%s" not found' % args.n, file=sys.stderr)
        sys.exit(1)

try:
    fd = os.open(str(args.d), os.O_RDWR)
except IOError as e:
    eprint('error: open "%s"; ' % args.d, e)
    sys.exit(1)

timestamp_file = args.d

if args.e:
    edid = i2c_read(fd, EDID_ADDR, 128)
    if edid[0:8] != bytes.fromhex("00ffffff ffffff00"):
        eprint("EDID retry")
        edid = i2c_read(fd, EDID_ADDR, 128)
        if edid[0:8] != bytes.fromhex("00ffffff ffffff00"):
            raise IOError("invalid EDID")
    print(len(edid), codecs.encode(edid, 'hex'))

ddc = DDC(fd, args.v)

if args.c or args.dump:
    caps = ddc.caps()
    if args.c:
        print(caps)
    if args.dump:
        ctrl_tab = ddc.parse_caps(caps)
        for page, ctrls in ctrl_tab:
            print("page 0x%02x" % page)
            for c in ctrls:
                try:
                    v, m = ddc.read_ctrl(page, c)
                    print("  %02x = %5d (max %5d)" % (c, v, m))
                except IndexError:
                    print("  %02x       (unsupported)" % c)

if args.control:
    if args.value:
        if args.v:
            eprint("write page %d control %d = %d" % (args.p, args.control, args.value))
        ddc.write_ctrl(args.p, args.control, args.value)
    else:
        val, maxval = ddc.read_ctrl(args.p, args.control)
        if args.v:
            eprint("page %d control %d: value=%d, max=%d" % (args.p, args.control, val, maxval))
        if args.i:
            val = min(max(0, val + args.i), maxval)
            if args.v:
                eprint("write page %d control %d = %d" % (args.p, args.control, val))
            ddc.write_ctrl(args.p, args.control, val)
        if not args.v:
            print(val)

os.close(fd)
